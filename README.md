# README #
# proj3-JSA
## **Author Information**

* Name:  Initial version by M Young; Docker version added by R Durairajan; revised by Kevin Post

* Contact address: Kpost7@uoregon.edu

## **Program description**

This is a basic Ajax application written in Python using JQuery and Flask to make an Ajax application, and hosted in a docker container.  

A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words.
